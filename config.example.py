SENTRY_ENABLED = False
SENTRY_DSN = ""

INSTANCE_NAME = "Smartcog"
INSTANCE_URL = ""
INSTANCE_CONTACTS = {
    "email": "",
}
GENERAL_INFO = {}

SUPPORTED_LANGUAGES = ["English"]
UNSUPPORTED_LANGUAGES = ["Greek"]

PORT = 8081

UPSTREAM_LINE_PULL_URL = "not_provided_here_check_docs"
UPSTREAM_ROUTE_PULL_URL = "not_provided_here_check_docs"
UPSTREAM_VEHICLE_PULL_URL = "not_provided_here_check_docs"

TIMINGS = {
    "lines": 300,
    "routes": 120,
    "stops": 15,
    "vehicles": 15,
    "update_schedules": 100
}

# Meta Information
GIT_URL = "https://gitlab.com/gtsatsis/smartcog"
VERSION = 0.1
