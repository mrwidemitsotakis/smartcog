import config
import json
import aiohttp
import asyncio
import urllib.request
from bs4 import BeautifulSoup
import re
from quart import current_app as app


async def lines():
    async with aiohttp.ClientSession() as session:
        async with session.get(config.UPSTREAM_LINE_PULL_URL) as api_resp:
            app.db["lines"] = json.loads(await api_resp.text())
            print("[Scheduled Task]: Lines Updated")


async def routes():
    routes = []
    for line in app.db["lines"]:
        line_code = line["LineCode"]
        async with aiohttp.ClientSession() as session:
            async with session.get(
                f"{config.UPSTREAM_ROUTE_PULL_URL}{line_code}"
            ) as api_resp:
                routes.append(json.loads(await api_resp.text()))

    app.db["routes"] = routes
    print("[Scheduled Task]: Routes Updated")


async def stops():
    pass


async def vehicles():
    vehicles = []
    for route in app.db["routes"]:
        route_code = route[0]["route_code"]
        async with aiohttp.ClientSession() as session:
            async with session.get(
                f"{config.UPSTREAM_VEHICLE_PULL_URL}{route_code}"
            ) as api_resp:
                for vehicle in json.loads(await api_resp.text()):

                    vehicles.append(
                        {
                            "id": int(vehicle["VEH_NO"]),
                            "last_seen": vehicle["CS_DATE"],
                            "latitude": str(vehicle["CS_LAT"]),
                            "longtitude": str(vehicle["CS_LNG"]),
                            "route": int(vehicle["ROUTE_CODE"]),
                        }
                    )
    app.db["vehicles"] = vehicles
    print("[Scheduled Task]: Vehicles Updated")

#Todo: move this to a regex functions class when more regex wizardy comes
cleanr = re.compile(r'<.*?>')
async def cleanhtml(raw):
    new_raw = []
    for raw_html in raw:   
        new_raw.append(cleanr.sub('',str(raw_html)))
    return new_raw

async def metram_schedules():    
    source = urllib.request.urlopen('http://stasy.gr/index.php?id=68').read()
    soup = BeautifulSoup(source,'html.parser')
    tables = soup.find_all('table')
    schedules = []
    for table in tables:
        route_fwd = []
        route_bck = []
        table_rows = table.find_all('tr')
        for tr in table_rows:
            table_items = tr.find_all('td')
            if len(table_items) == 4:
                clean_table_itms = await cleanhtml(table_items)
                route_fwd.append({"station":  clean_table_itms[0],"time": clean_table_itms[1]})
                route_bck.append({"station":  clean_table_itms[2],"time": clean_table_itms[3]})
        schedules.append({"forward": route_fwd,"backwards": route_bck})
    app.db["schedules"] = schedules
    print("[Scheduled Task]: Schedules Updated")