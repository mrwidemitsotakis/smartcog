import config
from quart import current_app as app
from smartcog.getters import routes, schedules


async def get_lines():
    lines = []

    for line in app.db["lines"]:
        lines.append(
            {
                "_id": int(line["LineCode"]),
                "id": line["LineID"],
                "name": line["LineDescrEng"],
                "routes": await routes.get_for_line(line["LineCode"]),
                "schedules": await schedules.get_for_line(line["LineCode"]),
            }
        )


async def get_lines_list():
    lines = []

    for line in app.db["lines"]:
        lines.append(
            {
                "_id": int(line["LineCode"]),
                "id": line["LineID"],
                "name": line["LineDescrEng"],
            }
        )
        # lines.append(line)
    return lines
