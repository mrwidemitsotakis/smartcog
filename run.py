import config
import sentry_sdk
import asyncio
from violet import JobManager
from violet.fail_modes import RetryNoMax

from smartcog import updater
from smartcog.getters import (
    lines as line_getters,
    routes as route_getters,
    stops as stop_getters,
    vehicles as vehicle_getters,
    schedules as schedule_getters,
)
from quart import (
    Quart,
    request,
    Response,
    jsonify,
    redirect,
)

app = Quart(__name__)

if config.SENTRY_ENABLED:
    sentry_sdk.init(dsn=config.SENTRY_DSN)

# Disable JSONify module from re-arranging our responses
app.config["JSON_SORT_KEYS"] = False


@app.before_serving
async def init_db():
    app.loop = asyncio.get_event_loop()
    app.sched = JobManager(loop=app.loop, context_function=app.app_context)
    app.db = {"lines": [], "routes": [], "stops": [], "vehicles": [], "schedules": []}

    await updater.metram_schedules()
    await updater.lines()
    await updater.routes()

    app.sched.spawn_periodic(
        updater.lines,
        [],
        period=config.TIMINGS["lines"],
        name="update_lines",
        fail_mode=RetryNoMax(),
    )
    app.sched.spawn_periodic(
        updater.routes,
        [],
        period=config.TIMINGS["routes"],
        name="update_routes",
        fail_mode=RetryNoMax(),
    )
    app.sched.spawn_periodic(
        updater.stops,
        [],
        period=config.TIMINGS["stops"],
        name="update_stops",
        fail_mode=RetryNoMax(),
    )
    app.sched.spawn_periodic(
        updater.vehicles,
        [],
        period=config.TIMINGS["vehicles"],
        name="update_vehicles",
        fail_mode=RetryNoMax(),
    )
    app.sched.spawn_periodic(
        updater.metram_schedules,
        [],
        period=config.TIMINGS["update_schedules"],
        name="update_schedules",
        fail_mode=RetryNoMax(),
    )


@app.errorhandler(500)
def handle_exception(exception):
    """Handle any kind of exception."""
    status_code = 500

    try:
        status_code = exception.status_code
    except AttributeError:
        pass

    sentry_sdk.capture_exception(exception)
    return (
        jsonify(
            {
                "success": False,
                "message": "There was an error, please let a system administrator know.",
            }
        ),
        status_code,
    )


@app.route("/")
async def index():
    return jsonify(
        {
            "instance_info": {
                "instance_name": config.INSTANCE_NAME,
                "instance_url": config.INSTANCE_URL,
                "supported_languages": config.SUPPORTED_LANGUAGES,
                "unsupported_languages": config.UNSUPPORTED_LANGUAGES,
            },
            "contact_info": config.INSTANCE_CONTACTS,
            "general_info": config.GENERAL_INFO,
        }
    )


@app.route("/lines")
async def get_lines():
    pass


@app.route("/lines/list")
async def get_lines_list():
    return jsonify(await line_getters.get_lines_list())


@app.route("/line/<line_id>")
async def get_line_single(line_id):
    pass


@app.route("/routes")
async def get_routes():
    return jsonify(app.db["routes"])


@app.route("/route/<route_id>")
async def get_route_single(route_id):
    pass


@app.route("/dbg/vehicles/all")
async def get_all_vehicles():
    return jsonify(app.db["vehicles"])

@app.route("/schedule/all")
async def get_all_schedules():
    return jsonify(str(app.db["schedules"]))

@app.route("/schedule/calculate/trtime/<line_id>",methods=["GET"])
async def get_schedule_test(line_id):
    await request.get_data()
    line_id = int(line_id)
    start_station =  request.args['startstation']
    end_station =  request.args['endstation']
    return str( await schedule_getters.get_travel_time(line_id,start_station,end_station))

if __name__ == "__main__":
    app.run(port=config.PORT)
