## Smartcog

Smartcog is a fast, simple, and easy-to-use API and caching layer for the [OASA Telematics API](http://telematics.oasa.gr) initially developed because I didn't want to bother with the slowness of the official API and app while trying to catch a bus to get to school.

It is written in Python 3 using Quart.

### Installing

Please see [docs/installing.md](docs/installing.md) for installation documentation

### Usage

Please see [docs/general_use.md](docs/general_use.md) for general examples, as well as [docs/api_spec.yml](docs/api_spec.yml) for a more in-depth explanation of how the API works.

### Previous branches

Previous attempts at creating Smartcog were in PHP and extremely unoptimized as well as unable to offer the experience I wanted. I am rewriting this in Python to have it ready for next school year (heh, who knows, maybe I'll turn this in as my end-of-year, haha).